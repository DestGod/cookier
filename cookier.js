export function cookier(name, value, params) {

    let documentCookies = document.cookie.split(';')
        .map((cookie) => {
            let cookieArray = cookie.split('=');
            return {
                name: cookieArray[0].trim(),
                value: cookieArray[1]
            };
        });

    if (typeof name === 'undefined') {

        return documentCookies;

    } else {

        if (typeof value === 'undefined') {
            let cookieIndex = documentCookies.findIndex((cookie) => {
                return cookie.name === name
            });
            return typeof documentCookies[cookieIndex] === 'undefined' ? null : documentCookies[cookieIndex];
        } else {
            let cookie = `${name}=${value}`;

            if (typeof params !== 'undefined') {
                for (let [key, value] of Object.entries(params)) {
                    cookie += '; ';
                    cookie += `${key}=${value}`;
                }
            }

            if (document.cookie = cookie)
                return true;
            else
                return false;
        }

    }
}